﻿using UnityEngine;

public class OnOffIsObservedGroup : IsObservedGroup
{
    public Transform [] m_affectedRoot;
    public bool m_isVisible = false;

    public void Update()
    {
        RefreshState();
    }

    private void RefreshState()
    {
        if (m_affectedRoot == null)
            return;

        bool previous = m_isVisible;
        m_isVisible = ArePointsInView();
        if (previous != m_isVisible)
        {
            for (int i = 0; i < m_affectedRoot.Length; i++)
            {

                m_affectedRoot[i].gameObject.SetActive(m_isVisible);
            }
        }
    }
}
