﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerViewInfoTDD : MonoBehaviour
{
    public PlayerViewInfo m_linked;
    public Transform m_point;

    void Update()
    {
        if(m_point!=null)
        m_linked.DrawViewToObject(m_point, Color.blue, Color.red, Time.deltaTime);
        
    }
}
