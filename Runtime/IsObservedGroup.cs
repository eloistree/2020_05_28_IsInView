﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsObservedGroup : MonoBehaviour
{
    public Transform[] m_points;

    public bool ArePointsInView()
    {
        if (PlayerViewInfo.InScene != null)
        {
            for (int i = 0; i < m_points.Length; i++)
            {
                if (PlayerViewInfo.InScene.IsInView(m_points[i]))
                    return true;
            }
        }
        return false;
    }

}

