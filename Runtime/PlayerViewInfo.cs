﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerViewInfo : MonoBehaviour
{
    private static PlayerViewInfo m_inScene;

    public static PlayerViewInfo InScene
    {
        get { return m_inScene; }
        private set { m_inScene = value; }
    }


    public Transform m_direction;
    [Range(0,180)]
    public float m_viewAngle=110;

    public Vector3 GetPosition()
    {
        return m_direction.position;
    }

    public bool m_useDebug;

    public void Awake()
    {
        m_inScene = this;
    }

    public void Update()
    {
        if (m_useDebug)
        {
            DrawLine(Time.deltaTime);

        }
    }
    //private void OnValidate()
    //{
    //    DrawLine();
    //}
    private void DrawLine(float time =1)
    {
        float halfAngle = -(m_viewAngle -180f)/2f;
        float xAngle = Mathf.Deg2Rad*( 180 - halfAngle);
        float yAngle = Mathf.Deg2Rad*(halfAngle);
        Vector3 right = new Vector3(GetX(xAngle), 0, GetY(xAngle));
        Vector3 left = new Vector3(GetX(yAngle), 0, GetY(yAngle));
        Vector3 top = new Vector3(0, GetX(xAngle), GetY(xAngle));
        Vector3 down = new Vector3(0, GetX(yAngle), GetY(yAngle));

        Debug.DrawLine(m_direction.position, m_direction.position + m_direction.forward * 2f, Color.magenta, time);
        DrawLineInDirection(right, time);
        DrawLineInDirection(left, time);
        DrawLineInDirection(top, time);
        DrawLineInDirection(down, time);
    }

    public bool IsInView(Transform point)
    {
        Vector3 pointLocaly=  m_direction.InverseTransformPoint(point.position);
        float angle = Vector3.Angle(Vector3.forward, pointLocaly);
        return angle < m_viewAngle * 0.5f;

    }
    public void DrawViewToObject(Transform obj, Color color, float timeDelta)
    {
        DrawViewToObject(obj, color, timeDelta);
    }
    public void DrawViewToObject(Transform obj, Color colorIn, Color colorOut, float timeDelta) {
        bool isIn = IsInView(obj);
        Debug.DrawLine(m_direction.position, obj.position, isIn?colorIn: colorOut, timeDelta);
    }

    private void DrawLineInDirection(Vector3 direction, float time)
    {
        Debug.DrawLine(m_direction.position, m_direction.position +
            m_direction.TransformDirection(direction * 2f), Color.green, time);
    }

    private float GetY(float angle)
    {
        return Mathf.Sin(angle);
    }

    private float GetX(float angle)
    {
        return Mathf.Cos(angle);
    }

    private void Reset()
    {
        m_direction = GetComponent<Transform>();
    }

}
